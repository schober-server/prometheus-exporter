#!/bin/sh

path=$(dirname $(realpath $0))

cd $path

echo $path

docker compose build --pull

$path/restart.sh
